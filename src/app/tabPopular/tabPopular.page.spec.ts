import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabPopularPage } from './tabPopular.page';

describe('TabPopularPage', () => {
  let component: TabPopularPage;
  let fixture: ComponentFixture<TabPopularPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabPopularPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabPopularPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
