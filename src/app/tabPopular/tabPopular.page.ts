import { Component } from '@angular/core';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-tabPopular',
  templateUrl: 'tabPopular.page.html',
  styleUrls: ['tabPopular.page.scss'],
  providers: [MovieService]
})
export class TabPopularPage {

  public listaFilmes: Array<any>;

  constructor(public movieService: MovieService) {
    this.ionViewDidLoad();
  }

  ionViewDidLoad() {
    this.movieService.getPopularMovie().subscribe(
      data => {
        const obj = (data as any);
        const obj_json = JSON.parse(obj._body);
        this.listaFilmes = obj_json.results;
      }, error => {
        console.log(error);
      }
    )
  }

}
