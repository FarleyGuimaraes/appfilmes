import { Component } from '@angular/core';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-tabTop',
  templateUrl: 'tabTop.page.html',
  styleUrls: ['tabTop.page.scss'],
  providers: [MovieService]
})
export class TabTopPage {

  public listaFilmes: Array<any>;

  constructor(public movieService: MovieService) {
    this.ionViewDidLoad();
  }

  ionViewDidLoad() {
    this.movieService.getTopRateMovie().subscribe(
      data => {
        const obj = (data as any);
        const obj_json = JSON.parse(obj._body);
        this.listaFilmes = obj_json.results;
      }, error => {
        console.log(error);
      }
    )
  }
}
