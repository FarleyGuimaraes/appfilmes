import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabTopPage } from './tabTop.page';

describe('Tab3Page', () => {
  let component: TabTopPage;
  let fixture: ComponentFixture<TabTopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabTopPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabTopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
