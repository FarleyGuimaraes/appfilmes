import { Component } from '@angular/core';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-tabCartaz',
  templateUrl: 'tabCartaz.page.html',
  styleUrls: ['tabCartaz.page.scss'],
  providers: [MovieService]
})
export class TabCartazPage {

  public listaFilmes: Array<any>;

  constructor(public movieService: MovieService) {
    this.ionViewDidLoad();
  }

  ionViewDidLoad() {
    this.movieService.getNowPlayingMovie().subscribe(
      data => {
        const obj = (data as any);
        const obj_json = JSON.parse(obj._body);
        this.listaFilmes = obj_json.results;
      }, error => {
        console.log(error);
      }
    )
  }
}
