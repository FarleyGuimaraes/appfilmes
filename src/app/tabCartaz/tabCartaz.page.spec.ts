import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabCartazPage } from './tabCartaz.page';

describe('TabCartazPage', () => {
  let component: TabCartazPage;
  let fixture: ComponentFixture<TabCartazPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabCartazPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabCartazPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
