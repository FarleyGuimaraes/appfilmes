import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tabCartaz',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabCartaz/tabCartaz.module').then(m => m.TabCartazPageModule)
          }
        ]
      },
      {
        path: 'tabPopular',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabPopular/tabPopular.module').then(m => m.TabPopularPageModule)
          }
        ]
      },
      {
        path: 'tabTop',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabTop/tabTop.module').then(m => m.TabTopPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tabCartaz',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tabCartaz',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
