import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
    providedIn: 'root'
})
export class MovieService {

    private baseUrl: string = "https://api.themoviedb.org/3/movie/";

    constructor(public http: Http) {

    }

    public getNowPlayingMovie() {
        return this.getMovie("now_playing");
    }

    public getPopularMovie() {
        return this.getMovie("popular");
    }

    public getTopRateMovie() {
        return this.getMovie("top_rated");
    }

    private getMovie(categoria: string) {
        return this.http.get(this.baseUrl + categoria + this.getApiKey() + "&language=pt-BR");
    }

    private getApiKey(): string {
        return "?api_key=43964cdbd7f3d015d80dc6c64f7a9c9c";
    }
}
